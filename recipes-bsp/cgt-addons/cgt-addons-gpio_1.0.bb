# Congatec QMX6 GPIO
inherit update-rc.d

LICENSE = "GPLv2"
LIC_FILES_CHKSUM = "file://gpio.sh;md5=4585af1532d83f610c52a55f0955ce03"

SRC_URI = " file://gpio.sh "
S = "${WORKDIR}"

DEPENDS_append = " update-rc.d-native"

do_configure() {
        :
}

do_compile() {
        :
}

do_install(){
	install -d ${D}${sysconfdir}/init.d
	install -m 0755 ${WORKDIR}/gpio.sh ${D}${sysconfdir}/init.d

	update-rc.d -r ${D} gpio.sh start 99 .
}

INITSCRIPT_NAME = "gpio.sh"

PACKAGE_ARCH = "${MACHINE_ARCH}"
COMPATIBLE_MACHINE = "(cgtqmx6|cgtumx6|cgtimx6)"
