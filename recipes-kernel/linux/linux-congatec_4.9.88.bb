# Congatec QMX6 Linux/kernel

LICENSE = "GPLv2"

require recipes-kernel/linux/linux-imx.inc

DEPENDS += "lzop-native bc-native"

SRCBRANCH = "cgt_imx_4.9.88_2.0.0"

SRC_URI = "git://git.congatec.com/arm/imx6_kernel_4.9.git;protocol=http;branch=${SRCBRANCH} \
           file://defconfig \
	   "
SRCREV = "97493919ce796c79d60133b6bd79437db29b4660"

COMPATIBLE_MACHINE = "(cgtqmx6|cgtumx6|cgtimx6)"
