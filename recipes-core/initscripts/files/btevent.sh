#!/bin/sh
### BEGIN INIT INFO
# Provides:             btevent
# Required-Start:
# Required-Stop:
# Default-Start:        S
# Default-Stop:
### END INIT INFO

if [ -f /usr/bin/btevent ]; then
        /usr/bin/btevent -m -p
fi