FILESEXTRAPATHS_prepend := "${THISDIR}/files:"

SRC_URI_append = " file://btevent.sh "

do_install_append() {
	install -d ${D}${sysconfdir}
	install -m 0755 ${WORKDIR}/btevent.sh ${D}${sysconfdir}/init.d
	update-rc.d -r ${D} btevent.sh start 99 S .
}
