#@TYPE: Machine
#@NAME: Congatec QMX6 Evaluation board
#@SOC: i.MX6 Q/DL
#@DESCRIPTION: Machine configuration for Congatec QMX6 Evaluation board
#@MAINTAINER: Alex de Cabo <alejandro.de-cabo-garcia@congatec.com>

MACHINEOVERRIDES =. "mx6:mx6dl:mx6q:"

include conf/machine/include/imx-base.inc
include conf/machine/include/tune-cortexa9.inc

PREFERRED_PROVIDER_u-boot ?= "u-boot-congatec"
PREFERRED_PROVIDER_virtual/bootloader_cgtimx6 ?= "u-boot-congatec"
UBOOT_MAKE_TARGET = ""
UBOOT_SUFFIX = "img"
SPL_BINARY = "SPL"
UBOOT_CONFIG ??= " \
        cgtumx6 \
        cgtqmx6 \
"

UBOOT_CONFIG[cgtumx6] = "cgtumx6_defconfig"
UBOOT_CONFIG[cgtqmx6] = "cgtqmx6eval_defconfig"
WKS_FILE = "imx-uboot-spl-bootpart.wks"

# Use linux kernel QMX6
PREFERRED_PROVIDER_virtual/kernel = "linux-congatec"
PREFERRED_PROVIDER_kernel = "linux-congatec"
KERNEL_DEVICETREE = "imx6q-qmx6.dtb imx6dl-qmx6.dtb imx6q-umx6.dtb imx6dl-umx6.dtb"
KERNEL_IMAGETYPE = "uImage"
PREFERRED_VERSION_linux-congatec = "4.9.88"

SERIAL_CONSOLE = "115200 ttymxc1"

MACHINE_FEATURES += " pci"
MACHINE_HAS_VIVANTE_KERNEL_DRIVER_SUPPORT = "1"
MACHINE_USES_VIVANTE_KERNEL_DRIVER_MODULE = "0"

#Create only tar.bz2 image
IMAGE_FSTYPES = "tar.bz2"

IMAGE_INSTALL_append += " kernel-modules kernel-image kernel-devicetree cgt-addons-btevent cgt-addons-gpio e2fsprogs-mke2fs rpm pciutils util-linux-sfdisk can-utils libsocketcan cpuburn-neon cpufrequtils "
